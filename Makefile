SHELL=/bin/bash
req:
	pipenv shell --anyway && pipenv install

.PHONY: search
search:
	pipenv shell --anyway && python hntagsearch.py
