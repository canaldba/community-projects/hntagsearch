from bs4 import BeautifulSoup
import requests

#List of keywords to search on hn articles
KEYWORDS=['mongo','data','dbms','mysql','postgres','relational','sql','nosql']
LIMIT=10

baseUrl = "https://news.ycombinator.com/"
baseUrlFront = "https://news.ycombinator.com/news"
baseUrlNewest = "https://news.ycombinator.com/newest"
params = dict()

def get_url(ix):
    url = baseUrl + "?p={0}".format(str(ix))
    return url

def get_page(ix):
    # url = get_url(ix)
    r = requests.get(
        baseUrlFront,
        params={"p":str(ix)}
    )
    page = BeautifulSoup(r.text,features="html.parser")
    return page

def get_newest_page(href=None):
    if href is None:
        r = requests.get(baseUrlNewest)
    else:
        r = requests.get(baseUrl + str(href))

    try:
        pageParsed = BeautifulSoup(r.text,features="html.parser")
        hrefs = pageParsed.find_all('a', class_='morelink')[0]
        oc = look_for_keyword(pageParsed,KEYWORDS)
        if oc is not None:
            print(oc)
    except IndexError as i:
        print("No hay más posts")
        return 0

    get_newest_page(hrefs['href'])

def look_for_keyword(page,word_list=None):
    for article in page.find_all('a', class_='storylink'):
        for word in word_list:
            if word.lower() in article.get_text().lower():
                if article['href'] is not None:
                    return ("{0} ({1})".format(article.get_text(), article['href']) )

def main():
    # Front Page
    for ix in range(1,LIMIT):
        page = get_page(ix)
        print (look_for_keyword(page,KEYWORDS))
    
    print("")
    print("By New")
    # Recursive
    get_newest_page()

if __name__ == '__main__':
    main()
