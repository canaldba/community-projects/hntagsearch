import unittest
from hntagsearch import look_for_keyword, get_page

class TestKeywords(unittest.TestCase):
    def test_hn(self):
        url='https://web.archive.org/web/20180312000019/https://news.ycombinator.com/'
        page=get_page(url)
        keywords=['vim']
        result = look_for_keyword(page,keywords)
        expected_result = "Vim Clutch – A hardware pedal for improved text editing (2012) (https://web.archive.org/web/20180312000019/https://github.com/alevchuk/vim-clutch)"
        self.assertEqual(result, expected_result)

if __name__== '__main__':
    unittest.main()
